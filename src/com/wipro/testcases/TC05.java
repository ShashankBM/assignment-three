package com.wipro.testcases;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Properties;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.ClickAction;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import jxl.Cell;
import jxl.Range;
import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;
import jxl.read.biff.PasswordException;
import net.bytebuddy.description.annotation.AnnotationDescription.Loadable;

public class TC05 {
	WebDriver dr;
	
	@BeforeClass
	public void open() throws FileNotFoundException, IOException, InterruptedException
	{
		
        dr=new ChromeDriver();
       	}
	
	@Test
	public void test1() throws FileNotFoundException, IOException, InterruptedException
	{
		Properties properties=new Properties();
        properties.load(new FileInputStream("D:\\20111917\\ASSIGNMENT\\resources\\config\\config.properties"));
        String url=properties.getProperty("url");
	        dr.get(url);
	        System.out.println(url);
	        Thread.sleep(5000);

	}
	@Test
	public void test2() throws InterruptedException
	{
		dr.findElement(By.xpath("//*[@id=\"top-links\"]/ul/li[2]/a")).click();
		dr.findElement(By.linkText("Login")).click();
		Thread.sleep(4000);
		
	}
	@Test
	public void test3() throws BiffException, IOException
	{
		 File f=new File("D:\\20111917\\login.xls");
		  Workbook b=Workbook.getWorkbook(f);
		Sheet s=b.getSheet(0);
		int col=s.getColumns();
		String []str=new String[col];
		for(int i=0;i<col;i++)
		{
			Cell c=s.getCell(i, 0);
			str[i]=c.getContents();
		}
		dr.findElement(By.xpath("//*[@id=\"input-email\"]")).sendKeys(str[0]);
		dr.findElement(By.xpath("//*[@id=\"input-password\"]")).sendKeys(str[1]);
		
	}
	@Test
	public void test4() throws IOException
	{
		dr.findElement(By.xpath("//*[@id=\"content\"]/div/div[2]/div/form/input")).click();
		  File screenshotFile = ((TakesScreenshot)dr).getScreenshotAs(OutputType.FILE);
	      FileUtils.copyFile(screenshotFile, new File("D:\\20111743\\ASSIGNMENT3\\screenshots\\TC05_04.png"));
	 	
	}
	@Test
	public void test5() 
	{
		dr.findElement(By.linkText("Your Store")).click();
		
		
	}
	@Test
	public void test6()
	{
		dr.findElement(By.linkText("Brands")).click();
	}
	
	@Test
	public void test7() throws IOException
	{
		for(int i=1;i<=5;i++) {
			dr.findElement(By.xpath("//*[@id=\"content\"]/div["+i+"]/div/a")).click();
			File screenshotFile1 = ((TakesScreenshot)dr).getScreenshotAs(OutputType.FILE);
		    FileUtils.copyFile(screenshotFile1, new File("D:\\20111917\\ASSIGNMENT\\resources\\screenshots\\TC05_7"+i+".png"));
			dr.navigate().back();
		}
	}
	@Test
	public void test8() throws InterruptedException
	{
		dr.findElement(By.xpath("//*[@id=\"top-links\"]/ul/li[2]/a/i")).click();
		Thread.sleep(2000);
	}
	
	@Test
	public void test9() throws IOException
	{
		dr.findElement(By.linkText("Logout")).click();
		BufferedWriter writer = new BufferedWriter(new FileWriter("success.txt",true));
	     writer.write(dr.findElement(By.xpath("//*[@id=\"content\"]/h1")).getText());
	     writer.close();
	}
	@AfterClass
	public void AfterClass() {
		dr.close();
	}
	
}