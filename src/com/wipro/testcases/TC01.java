package com.wipro.testcases;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Properties;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import jxl.Cell;
import jxl.Range;
import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;
import jxl.read.biff.PasswordException;
import net.bytebuddy.description.annotation.AnnotationDescription.Loadable;

public class TC01 {
	WebDriver dr;
	
	@BeforeClass
	public void open() throws FileNotFoundException, IOException, InterruptedException
	{
		
        dr=new ChromeDriver();
       	}
	
	@Test
	public void test1() throws FileNotFoundException, IOException, InterruptedException
	{
		Properties properties=new Properties();
        properties.load(new FileInputStream("D:\\20111917\\ASSIGNMENT\\resources\\config\\config.properties"));
        String url=properties.getProperty("url");
	        dr.get(url);
	        System.out.println(url);
	        Thread.sleep(5000);

	}
	@Test
	public void test2() throws InterruptedException
	{
		dr.findElement(By.xpath("//*[@id=\"top-links\"]/ul/li[2]/a")).click();
		dr.findElement(By.linkText("Register")).click();
		Thread.sleep(4000);
		
	}
	
	@Test
	public void test3() throws BiffException, IOException, InterruptedException {
		  File f=new File("D:\\20111917\\Registration.xls");
		  Workbook b=Workbook.getWorkbook(f);
		Sheet s=b.getSheet(0);
		int col=s.getColumns();
		String []str=new String[col];
		for(int i=0;i<col;i++)
		{
			Cell c=s.getCell(i, 0);
			str[i]=c.getContents();
		}
		dr.findElement(By.name("firstname")).sendKeys(str[0]);
		dr.findElement(By.name("lastname")).sendKeys(str[1]);
		dr.findElement(By.name("email")).sendKeys(str[2]);
		dr.findElement(By.name("telephone")).sendKeys(str[3]);
		dr.findElement(By.name("password")).sendKeys(str[4]);
		dr.findElement(By.name("confirm")).sendKeys(str[5]);
		Thread.sleep(2000);
			}
	@Test
	public void test4() throws IOException, InterruptedException
	{
		
		dr.findElement(By.xpath("//*[@id=\"content\"]/form/div/div/input[1]")).click();
			BufferedWriter writer = new BufferedWriter(new FileWriter("Policy.txt",true));
		     writer.write("\n Checkbox is checked");

	        writer.close();
		     Thread.sleep(2000);
		     dr.findElement(By.xpath("//*[@id=\"content\"]/form/div/div/input[2]")).click();
		}
		
		
	
	@Test
	public void test5() throws IOException
	{
		dr.findElement(By.xpath("//*[@id=\"content\"]/h1")).getText();
		BufferedWriter writer = new BufferedWriter(new FileWriter("Policy.txt",true));
	     writer.write(dr.findElement(By.xpath("//*[@id=\"content\"]/h1")).getText());
       writer.close();
       File screenshotFile = ((TakesScreenshot)dr).getScreenshotAs(OutputType.FILE);
       FileUtils.copyFile(screenshotFile, new File("D:\\20111917\\ASSIGNMENT\\screenshots\\TC03_5.png"));
		
	}
	
	@Test
	public void test6() throws InterruptedException
	{
		dr.findElement(By.linkText("My Account")).click();
		Thread.sleep(2000);
		dr.findElement(By.linkText("Logout")).click();
		Thread.sleep(2000);
	}
	
	
			
			
		


	@AfterClass
	public void close()
	{
		dr.close();
	}
}
